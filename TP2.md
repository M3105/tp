## Le DNS Complet

__La grille d'évaluation est en fin de fichier__

__Table des matières__

* [`PARTIE 0:  Mise en place`](#mep) (35 minutes)
* [`PARTIE 1:  La topologie`](#topo) (5 minutes)
* [`PARTIE 2:  Serveur d'Autorité`](#ad) (25 minutes)
* [`PARTIE 3:  Top Level Domain`](#tld) (20 minutes)
* [`PARTIE 4:  Root Server`](#rs)(15 minutes)
* [`PARTIE 5:  Aller plus loin`](#far)(80 minutes)
* [`Notes`](#note)
* [`Grille d'évaluation`](#grille)
* [`Exemples`](#examples)
---

---

__:repeat: L'objectif de ce TP :__  Au vu des rendus sur le DNS, il me semble que l'intérêt et la mise en place ne soit pas acquise pour tout le monde, pour ces personnes, ce TP est une seconde chance. Pour les autres vous allez découvrir d'autre choses comme:
- La mise en place du DNS complet de l'internet.
- L'utilisation d'un autre système `freebsd`


__:warning: Il faudra un compte rendu__. Rédiger votre compte rendu __en même temps__ que vous faites le TP. Les fichiers de configuration doivent être __aussi__ en ligne et accessibles. Il faut expliquer les fichiers de configurations. Faites un script de déploiement au moins pour la zone `iut.re`. Il me faut une section `troubleshouting`. 


---

<a name="mep"></a>
### PARTIE 0:  Mise en place
---

Dans cette partie vous allez mettre en place un simulateur/émulateur de réseau un peu comme packet filter mais avec quelques possibilités en plus. Ce simulateur s'appelle [IMUNES](https://github.com/imunes).  Il s'appuit sur de la virtualisation et utilise le système d'exploitation [freebsd](https://freebsd.org). 


##### Installation
Voici les étapes pour installer `imunes` sur votre système:
- Nous allons passer par une machine virtuelle dans laquelle `imunes` est déjà installé. Cette machine virtuelle est a télécharger ici [partage.rt-iut.re](http://partage.rt-iut.re/rtcloud/) depuis le réseau `rt` dans `logiciel/reseaux/imnues/` et choisissez `imunes-11.0-tah`. C'est un fichier `ova` qui s'installe facilement en faisant dans virtualbox `Fichier\Importer Un Appareil Virtuel`. 
- Dans virtualbox, vous devez créer une interface de management. Dans `Fichier\Host Network Manager`et rajouter une interface si elle n'existe pas. Cette interface doit avoir un nom du genre `vboxnet`
- Avant de démarrer la machine `imunes` vérifier la configuration réseau/ram etc... pas besoin spécifiquement d'un accès par pont dans notre cas.
- Lancer la machine virtuelle.  
- Pour changer la disposition du clavier lancer un terminal et lancer la commande: `setxkbmap -layout fr`
- Les mots de passe sont `root:imune` si besoin, mais normalement vous n'en avez pas besoin. 

##### Premiers tests
- Télécharger le squelette du réseau que nous allons utiliser. Il s'appelle [mydns2.imn](https://gitlab.com/M3105/tp/raw/master/files/mydns2.imn?inline=false). Sauvegarder le dans `/root` par exemple.  C'est un fichier imunes que vous pouvez télécharger avec la commande `wget https://gitlab.com/M3105/tp/raw/master/files/mydns2.imn` depuis un terminal. 
- Lancer l'application `imunes` dans la machine virtuelle en double-cliquant sur l'icone `imunes`. Dans l'interface d'imunes cliquez sur `file/open` et ouvrez le fichier que vous venez de télécharger. Vous devriez obtenir la topologie suivante.
- :warning: l'icone pour lancer imune est le suivant:
<img height="42" width="42" src="https://gitlab.com/M3105/tp/raw/master/files/icone.png?inline=false">

<img src="https://gitlab.com/M3105/tp/raw/master/files/mydns2.png?inline=false">

- Cette topologie est assez clair, mais nous reviendrons dans les détails plus tard dans ce TP si besoin. 
- Pour démarrer toutes les machines, et l'expérimentation dans la fenêtre `imunes`, cliquez sur `Experiment/Execute`. Les machines et les routeurs démarrent en quelques secondes. 
	- Le routage est configuré avec le protocole RIP sur les routeurs
	- Sur chaque serveur ou PC vous avez un système `freebsd` qui tourne. En faisant un clic droit par exemple sur `pc1` vous disposez de certaines options. Ce qui nous intéresse pour le moment: clic droit sur `pc1`puis `Shell window/bash` pour obtenir un shell sur `pc1`. Les commandes classiques unix fonctionnent normalement (`ifconfig, netstat, route`). 
	- Pour tester la topologie vous pouvez faire depuis le shell de `pc1`: `ping 10.0.0.10`pour un ping vers `aRootServer`. 

<a name="topo"></a>
### PARTIE 1:  La topologie
---

Le topologie est composé de:
- Un serveur racine `.` à configurer. Normalement il y a en a 13 comme vu en cours, ici nous n'allons en configurer qu'un seul c'est le `aRootServer`
- Il y a deux `TLD` (Top Level Domain) à configurer celui du `.re` et celui de `.org` sur les machines `dre` et  `dorg` respectivement. 
- Il y a 2 `AD` (Authoritative Domain) à configurer `wiki.org`, et `iut.re` sur `dwikiorg`,  et `diutre`respectivement. Il y a un domaine en délégation `rt.iut.re` gérer par le serveur `drtiutre`
- Dans le domaine `wiki.org` il y a la machine `www` à configurer et à inclure dans le domaine.
- Dans le domaine `rt.iut.re` il y a les machines `www`et `pc2`à configurer et inclure dans le domaine.
- Dans le domaine `iut.re` il y a les machines `www`et `pc1`à configurer et inclure dans le domaine.
- Dans chacun des domaines, vous mettrez aussi une `gateway`. 

Ce qu'il faut savoir:
- `bind` est déjà installé sur les machines. Je ne rentre pas dans le processus d'installation sur freebsd qui est aussi simple que sous linux. 
- En revanche il n'y a pas de fichiers d'exemples, tous les fichiers sont à créer. (les fichiers de zones, `named.conf` etc ...)
- Pour démarrer le serveur dns la commande est `named`. Pour éteindre vous pouvez faire un `killall -9 named`. La commande `named-checkconf` est disponible. Faites un `man` sur `named` pour plus d'infos. 
- Le fichier `named.conf` doit se trouver dans `/etc`. Si vous le créer à un autre endroit vous pouvez lancer la commande `named -c /chemin/vers/named.conf`

<a name="ad"></a>
### PARTIE 2:  Serveur d'autorité
---

Normalement cette partie est exactement la même que celle que vous avez faîtes dans le TP précédent. Elle n'a donc plus de secret pour vous et devrez être faîte rapidement. Dans cette section nous configurons le domaine `wiki.org` sur le serveur `dwikiorg` de la topologie. 
- Ce serveur détient l'adresse IP `10.0.7.10/24`, cette adresse est déjà affecté en statique.
- Pas besoin d'éditer le fichier `/etc/host` et le `hostname` est déjà configuré correctement. 
- Créer le répertoire `/etc/named`qui va contenir les configuration des zones avec `mkdir -p /etc/named`
- Editer le fichier `/etc/named.conf` (vous devez le créer si besoin). 
```bash
options {
	directory "/etc/named";
};

zone "." {
	type hint;
	file "named.root";
};

zone "0.0.127.IN-ADDR.ARPA" {
	type master;
	file "localhost.rev";
};

zone "wiki.org" {
 	type master;
 	file "wiki.dir";
};

zone "7.0.10.IN-ADDR.ARPA" {
 	type master;
 	file "wiki.rev";
};
```
- Tester si votre fichier `/etc/named.conf` est correct avec la commande `named-checkconf /etc/named.conf` 
- Il y a 4 zones que vous devez créer ici: `.`, `
0.0.127.IN-ADDR.ARPA`, `wiki.org` et `7.0.10.IN-ADDR.ARPA`. Vous devez créer les fichiers correspondant : `/etc/named/named.root`, `/etc/named/localhost.rev`, `/etc/named/wiki.dir` et `/etc/named/wiki.rev`. 
- le fichier `/etc/named/named.root` contient l'adresse du serveur racine.
```bash
$TTL 60000
@ IN SOA dwikiorg.wiki.org. root.dwikiorg.wiki.org. (
        2002102801 	; serial
        28 		; refresh
        14 		; retry
        3600000 	; expire
        0 		; default_ttl
)

.        	3600000 IN NS aRootServer.
aRootServer. 	3600000    A  10.0.0.10
```
- le fichier `/etc/named/localhost.rev` 
```bash
$TTL	86400
@	IN	SOA	localhost. root.localhost (
	20041128 ; Serial
	28800	 ; Refresh
	7200	 ; Retry
	3600000	 ; Expire
	86400 	 ; Minimum
)
	IN	NS	localhost.
1 	IN 	PTR 	localhost.
```

- le fichier `/etc/named/wiki.dir` 
```bash
$TTL 60000
@ IN SOA dwikiorg.wiki.org. root.dwikiorg.wiki.org (
        2002102801 	; serial
        28 		; refresh
        14 		; retry
        3600000 	; expire
        0 		; default_ttl
)
wiki.org.   IN  NS  	dwikiorg.wiki.org.
wiki.org.   IN  A  	10.0.7.10
dwikiorg    IN  A   	10.0.7.10
www         IN  A       10.0.7.11
gateway     IN  A   	10.0.7.1
```

- `/etc/named/wiki.rev`
```bash
$TTL 60000
@ IN SOA dwikiorg.wiki.org. root.dwikiorg.wiki.org (
        2002102801 	; serial
        28 		; refresh
        14 		; retry
        3600000 	; expire
        0 		; default_ttl
)
@	    IN  NS  	dwikiorg.wiki.org.
1	    IN	PTR	gateway.wiki.org.
10          IN  PTR   	dwikiorg.wiki.org.
11          IN  PTR	www.wiki.org.
```
- Testez chaque fichier avec la commande `named-checkconf`
- La même procédure est à appliquer sur `rt.iut.re`. :warning: C'est une zone délégué par `iut.re`. Donc dans le serveur `diutre` vous devez avoir les lignes spécifiques (en plus des lignes classiques):

```bash
rt.iut.re.         	IN  NS  drtiutre.rt.iut.re.
drtiutre.rt.iut.re. 	IN  A 	10.0.6.10
```
- Bien sur vous devez aussi configurer le domaine `iut.re`.

<a name="tld"></a>
### PARTIE 3:  Top Level Domain
---
Une fois les authoritative configurés, il faut configurer les TLD, dans notre cas `.re` et `.org`. Configuration guidée. Nous allons procédé avec le domaine `.org` dont le serveur est `dorg`
- Ce serveur à l'adresse IP `10.0.2.10/24`, cette adresse est déjà affecté en statique.
- Pas besoin d'éditer le fichier `/etc/host` et le `hostname` est déjà configuré correctement. 
- Créer le répertoire `/etc/named`qui va contenir les configuration des zones. 
- Editer le fichier `/etc/named.conf` (vous devez le créer si besoin). 
```bash
options {
	directory "/etc/named";
};

zone "." {
	type hint;
	file "named.root";
};

zone "org" {
	type master;
	file "org";
};

zone "0.0.127.IN-ADDR.ARPA" {
	type master;
	file "localhost.rev";
};
```
ici le type `hint` signifie que l'on donne au serveur dns une liste de serveur racine.

- Tester si votre fichier `/etc/named.conf` est correct avec la commande `named-checkconf /etc/named.conf` 
- Il y a 3 zones que vous devez créer ici: `.`, `
0.0.127.IN-ADDR.ARPA` et `org`. Vous devez créer les fichiers correspondant : `/etc/named/named.root`, `/etc/named/localhost.rev` et `/etc/named/org`. 
- Le fichier `/etc/named/named.root` est sensiblement le même que celui de l'authoritative car il référence simplement les serveurs racines. (voir plus haut). 
- Le fichier  `/etc/named/localhost.rev` est lui aussi sensiblement le même que le précédent.
- Le fichier  `/etc/named/org`
```bash
$TTL 60000
@ IN SOA dorg.org. root.dorg.org (
        2002102801 	; serial
        28 		; refresh
        14 		; retry
        3600000 	; expire
        0 		; default_ttl
)

@		IN  NS    dorg.org.
dorg.org.	IN  A     10.0.2.10

wiki.org.	IN  NS    dwikiorg.wiki.org.
dwikiorg.wiki.org.	IN  A     10.0.7.10
```
Les deux dernières lignes du fichier indique qui gère le domaine `wiki.org` (nom du serveur et adresse ip)

<a name="rs"></a>
### PARTIE 4:  RootServer
---

Configuration guidée du serveur racine `aRootServer`. 
- Ce serveur à l'adresse IP `10.0.0.10/24`, cette adresse est déjà affecté en statique.
- Pas besoin d'éditer le fichier `/etc/host` et le `hostname` est déjà configuré correctement. 
- Créer le répertoire `/etc/named`qui va contenir les configuration des zones. 
- Editer le fichier `/etc/named.conf` (vous devez le créer si besoin). 
```bash
options {
	directory "/etc/named";
};

zone "." {
	type master;
	file "root";
};

zone "0.0.127.IN-ADDR.ARPA" {
	type master;
	file "localhost.rev";
};

zone "IN-ADDR.ARPA" {
 	type master;
 	file "in-addr.arpa";
};
```
- Tester si votre fichier `/etc/named.conf` est correct avec la commande `named-checkconf /etc/named.conf` 
- Il y a 3 zones que vous devez créer ici: `.`, `
0.0.127.IN-ADDR.ARPA` et `IN-ADDR.ARPA`. Vous devez créer les fichiers correspondant : `/etc/named/root`, `/etc/named/localhost.rev` et `/etc/named/in-addr.arpa`. 

- le fichier  `/etc/named/root`
```bash
$TTL   60000
@    IN      SOA     aRootServer. root.aRootServer (
         2002102801 ; serial
         28800 ; refresh
         14400 ; retry
         3600000 ; expire
         0 ; default_ttl
)

@               IN   NS    aRootServer.
aRootServer.    IN   A	   10.0.0.10

org.            IN   NS    dorg.org.
dorg.org.       IN   A     10.0.2.10

re.             IN   NS    dre.re.
dre.re.         IN   A     10.0.3.10
```
les quatre dernière ligne de ce fichier référence les domaine `.re` et `.org`. 

- le fichier `/etc/named/localhost.rev` 
```bash
$TTL	86400
@	IN	SOA	localhost. root.localhost (
	20041128 ; Serial
	28800	 ; Refresh
	7200	 ; Retry
	3600000	 ; Expire
	86400 	 ; Minimum
)
	IN	NS	localhost.
1 	IN 	PTR 	localhost.
```

- le fichier `/etc/named/in-addr.arpa`
```bash
$TTL   60000
@               IN      SOA     aRootServer. root.aRootServer (
         2002102801 ; serial
         28800 ; refresh
         14400 ; retry
         3600000 ; expire
         0 ; default_ttl
)

@			IN	NS	aRootServer.
10.0.0.10.in-addr.arpa.	IN	PTR	aRootServer.
10.3.0.10.in-addr.arpa.	IN	PTR	dre.re.
10.2.0.10.in-addr.arpa.	IN	PTR	dorg.org.
```

<a name="far"></a>
### PARTIE 5:  Aller plus loin
---
Vous pouvez faire n'importe quel point dans l'ordre que vous voulez. C'est dans cette partie que vous allez vous faire des points !!!
- Installer des serveurs esclaves pour `aRootServer` par exemple déployer un serveur `bRootServer` et `cRootServer` qui soit esclave de `aRootServer`. Montrer que ces serveurs prennent bien le relai en cas de problème. 
- Installer un serveur esclave pour `.org`. Montrer que ce serveur prend bien le relai en cas de problème et montrer les échanges sur `wireshark`.
- Déployer le domaine `.za`, `.com.za`, `edu.za`, `facebook.com.za` avec `www.facebook.com.za` et `univ.edu.za` avec `www.univ.edu.za` Vous pouvez étendre la topologie de la manière que vous voulez. 
- Montrer avec des captures `wireshark` les types de résolutions (`recursion yes` ou avec un `forwarder` dans les options)
- Mettre en place une attaque DNSSPOOFING.
- Ecrire un script qui permette de déployer un nouveau domaine sans avoir a éditer manuellement les fichiers de configuration.  Par exemple, si je veux installer un domaine wikipedia.org un script devrait produire les fichiers de configuration et autre en prenant simplement en paramètre les noms de serveur, les IP etc...
- 
<a name="note"></a>
### Notes
---
- Pour lancer un serveur web, (à automatiser avec himage et hcp)
	- créer un dossier `mkdir -p /tmp/www`, 
	- positionnez-vous dans ce dossier `cd /tmp/www`, 
	- créer une page `index.html`, avec le contenu de votre choix, 
	- lancer un serveur web python sur le port 80 avec : `python -m SimpleHTTPServer 80`. 
	- Vous pouvez naviguez sur le site.
- Sur `pc1`et `pc2`n'oubliez pas de modifier/créer le fichier `/etc/resolv.conf` (pour pc1)
```bash
domain iut.re
nameserver 10.0.5.10
```
- Effectuer les tests avec la commande `dig`, vous pouvez aussi essayer de capturer des paquets avec `wireshark`. Wireshark est disponible en faisant un clique droit sur le routeur / switch / serveur / pc puis en cliquant sur wireshark.
- Depuis et vers la machine virtuelle, vous pouvez copier des fichiers. Par exemple si vous télécharger un fichier nommé `myresolv.conf` depuis votre googledrive ou autre. Vous télécharger ce fichier en utilisant le navigateur de la machine virtuelle imunes. Si vous voulez copier ce fichier dans le logiciel `imunes` sur la machine de votre topologie appelé `pc1` vous utiliser la commande `hcp`:
```bash
hcp myresolv.conf pc1:/etc/resolv.conf
```
cette commande copie le fichier `myresolv.conf` dans `/etc/resolv.conf` de `pc1`
- Depuis la machine virtuelle, vous pouvez executer une commande sur une machine dans imunes sans avoir à lancer un shell. Pour cela vous utilisez la commande `himage`. Si vous voulez créer un répertoire `/etc/named` sur la machine `dwikiorg` vous pouvez executer la commande suivante depuis la machine virtuelle:
```bash
himage dwikiorg mkdir -p /etc/named
```
- Tester vos fichiers de configuration et de zone au fur et à mesure avec les commandes : `named-checkzone`et `named-checkconf`.
- Si vous voulez installer quelque chose sur la machine virtuelle, par exemple `nano` vous utilisez la commande `pkg install nano`


#### Quelques explications sur les fichiers

Soit le fichier suivant: 
```bash
$TTL 60000
@ IN SOA dorg.org. root.dorg.org (
        2002102801 	; serial
        28 		; refresh
        14 		; retry
        3600000 	; expire
        0 		; default_ttl
)

@		IN  NS    dorg.org.
dorg.org.	IN  A     10.0.2.10

wiki.org.	IN  NS    dwikiorg.wiki.org.
dwikiorg.wiki.org.	IN  A     10.0.7.10
```
###### Nous allons décortiquer la ligne:
```bash
@ IN SOA dorg.org. root.dorg.org (
```

- `@` est à remplacer par le nom de la zone qui se trouve dans `named.conf` dans notre cas `org`. 
- `IN` signifie que nous avons une configuration internet. Il y a d'autres types de configuration que je vous laisse découvrir.
- `SOA`signifie start of authority ce qui signifie que nous avons un domaine à gérer.
- `dorg.org.` qui doit absolument suivre le mot clé `SOA`est le nom du serveur DNS responsable de la zone. Notez ici que le nom se termine par un `.` Si il n'y a pas de point, le nom de la zone (raccourci par le `@`) est ajouté à la fin du nom. Du coup si vous oubliez le `.` le nom de votre serveur DNS sera `dorg.org.org`
- `root.dorg.org` est juste une adresse email. le premier point est transformé en `@` donc l'adresse email ici est: `root@dorg.org` si vous voulez mettre votre email écrivez simplement `monmail.gmail.com`

###### Sur les lignes suivantes:
```bash
@		IN  NS    dorg.org.
dorg.org.	IN  A     10.0.2.10
```
- `@`est a remplacé par le nom de la zone qui se trouve dans `named.conf`. Dans notre cas `org`.
- `IN` signifie que nous avons une configuration internet. Il y a d'autres types de configuration que je vous laisse découvrir.
- `NS`signifie NameServer et veut dire que l'information qui suit est le nom d'un serveur dns. Dans notre cas, comme nous commençons la ligne par `@`cela signifie que nous avons le nom du serveur pour le domaine `org`. 
- `dorg.org.` est le nom du serveur DNS. Encore une fois attention au point à la fin.
- Sur la ligne suivante nous avons l'association `nom - ip`, `dorg.org.` est le nom du serveur. Notez bien le `.` à la fin. 
- `IN` signifie que nous avons une configuration internet. Il y a d'autres types de configuration que je vous laisse découvrir.
- `A` signifie que ce qui suit est une adresse IPv4 (`AAAA` pour une adresse IPv6).
- `10.0.2.10` est l'adresse IP du serveur.


<a name="grille"></a>
### Grille d'évaluation
---

##### Forme (4 points)
- Est ce que le rapport contient un titre ?  
- Est ce que le rapport contient une table des matières ?
- Est ce que le rapport contient beaucoup de fautes d'orthographe (+3 par page) ?  
-  (subjectif) est ce que le rapport est facile à lire ? Par exemple: trop long; trop court, pas bien organisé, les phrases sont trop longues, le style d'écriture n'est pas assez ou trop technique, trop de répétitions etc.

##### Fond (4 points)
- Est ce que le contenu est facilement reproductible ?  
- Est ce les instructions sont faciles à suivre ?  
- Est ce que les commandes sont expliquées ?  
- Est ce que les fichiers, commandes, capture d'écran sont commentés ?
- Est ce qu'il y a une section troubleshouting ? 

##### Technicité  (4 points)
- Est ce que les serveurs d'autorité sont configurés ? (iut.re, rt.iut.re, wiki.org)
- Est ce que les TLD sont configurés ? (org, re)
- Est ce que le serveur racine est configuré ? (root)
- Est ce que des tests (et les résultats de fonctionnement) sont donnés ? (ping **et** dig et navigation sur un serveur web)
- Est ce que des captures wireshark des requêtes sont données ?
- Est ce qu'il y a des scripts de déploiement ? Sont-ils commentés ?

##### Les plus (8 points)
- Est ce que les serveurs esclave bRootServer et cRootServer sont installés et testé avec une désactivation du serveur principale ?  
- Est ce qu'un serveur esclave pour .org a été installé ?  
- Est ce le TLD .za est installé ? Est que com.za, edu.za, facebook.com.za avec www.facebook.com.za et univ.edu.za avec www.univ.edu.za sont installés ?
- Est ce que les captures / test de recursion et fowarder sont fait ?  
- Est ce que le DNSSPOOFING est implémenté ou au moins défini ?
- Est ce que l'ajout d'un nouveau domaine (authorité) est facile ? Par exemple les fichiers de configuration sont crées automatiquement et déployés automatiquement ? Si je veux installer un domaine wikipedia.org est ce qu'un script est fourni pour que je n'ai pas a copier tous les fichiers et les éditer à la main ?

<a name="exemples"></a>
### Exemples
---

- Un exemple de script de déploiement est disponible [ici](https://gitlab.com/M3105/tp/blob/master/files/conf/deploy.sh)
- La configuration du server `dwikiorg` est disponible aussi. Je ne sais pas si elle fonctionne. Il faudra probablement vérifier les paramètres et autres. [Voir ici](https://gitlab.com/M3105/tp/tree/master/files/conf/wiki.org)
<!--stackedit_data:
eyJoaXN0b3J5IjpbNjkzMDAyMzU4LDcwNDE1NzI1OCwtNDI4Nj
UxNjc5LC0xMzE2NjEwNSwxOTI4MDQ4MTE1LC0xMTgwNTU4MzQ5
LC0xMzU3MTIxODk3LC02MDA3NzA0NTgsLTQxODYwOTQxMywtMT
g0NDQ5NjQ4OCwxMDkwMDI1NjY4LDI2Njk2NjIyNCw5OTMzMTc3
OTYsLTg2MzQxMDIwNCwxNTEyODYzNzExLC0xMjQzMzA1NjU3LD
YyMTkwMjUxMywxOTIxNTQ5NjkzLC0xMjY2NTExMDg3LC0xMTgx
NjU2MDAyXX0=
-->