### Vrai DNS pour vous.

- https://en.wikipedia.org/wiki/OpenNIC
- https://www.opennic.org/ on peut enregistrer des domaines .free, .null, etc... 
- Il faut configurer le domaine 
- ici: https://wiki.opennic.org/opennic/tier2setup
- https://www.digitalocean.com/community/tutorials/how-to-setup-dnssec-on-an-authoritative-bind-dns-server--2 DNSSec
- https://ftp.isc.org/isc/dnssec-guide/dnssec-guide.pdf Guide dnssec
- https://www.c0ffee.net/blog/mail-server-guide/ ou https://www.vultr.com/docs/building-your-own-mail-server-with-freebsd-11 (mail server)
- https://obsigna.com/articles/1539726598.html (mail complet)
- https://forums.freebsd.org/threads/mail-server-tutorial.68051/ (mail server)
- https://calomel.org/dns_spoof.html (dsnspoof)
- https://calomel.org/thttpd.html (tiny http server)

- http://www.canyouseeme.org/ (voir si on peut acceder à la machine depuis l'extérieur)

DNS resolvers can be added in OS X via the `networksetup` command:

```
sudo networksetup -setdnsservers Wi-Fi 178.17.170.179 217.12.210.54

```

Insert name of network connection as appropriate. These resolvers will appear in resolv.conf as it is automatically generated, but direct edits to resolv.conf will not result in those resolvers being used. I assume the resolvers are stored in a plist somewhere; I will search and report back.

Edited to add: It looks like the XML plist file storing the DNS servers is `/Library/Preferences/SystemConfiguration/preferences.plist`.

The host file in /private/etc/hosts should work to force resolution for particular names (/etc is a symlink to /private/etc). Can you verify your syntax and that you are editing the correct file?

- be.libre / tahiry.pirate / login tahiry:classique
- du2c.pirate
- iut.pirate
- TMUX : https://tmuxcheatsheet.com/

<!--stackedit_data:
eyJoaXN0b3J5IjpbMTU1Nzk2MTM1NywtMzUyNjIwNjg4LC0xND
UwNzYxNTk3LC0xOTM5NTYzMTA3LC0xMzg2ODQ4NTE0LDE5OTA0
NTgxNzQsLTE2Mzk1Mjg1MDAsMjAzNzA4MjU2MCwtMTM0MjcwMD
MyLC0yOTIzMjQ5NDEsLTE4NDAwMjMxNjQsMjExNDkzODE5NSwz
MTQ2MTM1MDksMTM0NTk4MDA2MV19
-->